module faucet-svc

go 1.19

require (
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/ethereum/go-ethereum v1.10.26
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/portto/solana-go-sdk v1.22.1
	github.com/rubenv/sql-migrate v1.2.0
	gitlab.com/distributed_lab/ape v1.7.1
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/kit v1.11.1
	gitlab.com/distributed_lab/logan v3.8.1+incompatible
	golang.org/x/exp v0.0.0-20221114191408-850992195362
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1 // indirect
	github.com/Masterminds/squirrel v1.4.0 // indirect
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/btcsuite/btcd/btcec/v2 v2.3.2 // indirect
	github.com/certifi/gocertifi v0.0.0-20200211180108-c7c1fbc02894 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/deckarep/golang-set v1.8.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.1.0 // indirect
	github.com/eteu-technologies/borsh-go v0.3.2 // indirect
	github.com/eteu-technologies/golang-uint128 v1.1.2-eteu // indirect
	github.com/eteu-technologies/near-api-go v0.0.1 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/getsentry/sentry-go v0.7.0 // indirect
	github.com/go-errors/errors v1.0.1 // indirect
	github.com/go-gorp/gorp/v3 v3.0.2 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/jsonapi v0.0.0-20200226002910-c8283f632fb7 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/lib/pq v1.10.0 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/mr-tron/base58 v1.2.0 // indirect
	github.com/near/borsh-go v0.3.2-0.20220516180422-1ff87d108454 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pelletier/go-toml v1.9.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rjeczalik/notify v0.9.1 // indirect
	github.com/shirou/gopsutil v3.21.4-0.20210419000835-c7a38de76ee5+incompatible // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	github.com/spf13/afero v1.9.3 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.8.1 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/subosito/gotenv v1.2.0 // indirect
	github.com/textileio/go-log/v2 v2.1.3-gke-1 // indirect
	github.com/textileio/near-api-go v0.2.0 // indirect
	github.com/tklauser/go-sysconf v0.3.5 // indirect
	github.com/tklauser/numcpus v0.2.2 // indirect
	gitlab.com/distributed_lab/figure/v3 v3.1.2 // indirect
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
	gitlab.com/distributed_lab/running v1.6.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	golang.org/x/crypto v0.3.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
